#! /bin/sh

cd $(dirname $0)
# Existing virtual environment
if [ -d .venv ]; then
    echo "Found existing Python virtual environment."
    echo "Replace it with a new one?"
    while true; do
        read -p "[Y/N] " input
        if [ "$input" = "Y" ] || [ "$input" = "y" ]; then
            rm -rf .venv
            break
        elif [ "$input" = "N" ] || [ "$input" = "n" ]; then
            echo "Aborted."
            exit 0
        else
            echo "Unknown input. Try again."
        fi
    done
fi
# Create virtual environment
virtualenv .venv
. .venv/bin/activate
# Install Chrome crawler from Github
pip install https://github.com/lqf96/chrome-crawler/raw/master/dist/latest.tar.gz
