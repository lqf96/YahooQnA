#! /usr/bin/env python2.7

from __future__ import unicode_literals, print_function
import json, sys
from os import chdir
from os.path import dirname, abspath, exists

# Arguments
entries_per_page = 10
auto_save_threshold = 20

if __name__=="__main__":
    # Too few arguments
    if len(sys.argv)<3:
        print("Too few arguments!")
        print("Usage: "+sys.argv[0]+" [Keyword] [Result Amount]")
        exit(1)
    # Change directory
    proj_dir = dirname(abspath(__file__))
    chdir(proj_dir)
    # Activate virtual environment
    activate_this = ".venv/bin/activate_this.py"
    execfile(activate_this, dict(__file__=activate_this))
    from chrome_crawler import ChromeCrawler
    # Result array
    result_amount = int(sys.argv[2])
    if exists("data/"+sys.argv[1]):
        with open("data/"+sys.argv[1]) as f:
            results = json.load(f)
        while len(results)%entries_per_page!=0:
            results.pop()
    else:
        results = []
    # Create crawler
    c = ChromeCrawler(
        custom_root=proj_dir,
        entry="main",
        amd_mapping={
            "co": "https://cdn.bootcss.com/co/4.1.0/index.min"
        },
        keyword=sys.argv[1],
        begin_page=len(results)/entries_per_page+1
    )
    # Open file to write result
    @c.on("result")
    def on_result(result):
        results.append(result)
        print("\nReceived a new question, total %d.\n" % len(results))
        if len(results)%auto_save_threshold==0 or len(results)==result_amount:
            with open("data/"+sys.argv[1], "w") as f:
                print("\nSaving to file...\n")
                json.dump(results, f)
            if len(results)==result_amount:
                print("\nCrawling finished.\n")
                sys.exit(0)
    # Run crawler
    c.run()
