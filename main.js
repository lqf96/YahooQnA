//Main.js: Yahoo Q&A crawling frontend script
"use strict";

define(["co", "crawler"], (co, crawler) => {
    co(function*()
    {   var c = new crawler();
        //Fetch crawler configuration
        var conf = yield c.get();

        for (var i=conf.begin_page;i<=100;i++)
        {   //Get all question IDs
            var qids = yield c.crawl("https://answers.yahoo.com/search/search_result"+
                "?fr=uh3_answers_vert_gs&type=2button&p="+conf.keyword+"&s="+i,
                () => {
                    var q_rx = /^q-(.*)$/;
                    return Array.prototype.map.call(
                        document.querySelectorAll("#yan-questions li"),
                        (el) => (el.getAttribute("id").match(q_rx)[1])
                    );
                });

            //Fetch each question and answers
            for (var qid of qids)
                yield c.crawl("https://answers.yahoo.com/question/index?qid="+qid,
                (c, qid) => {
                    //Result object
                    var result = {
                        "qid": qid,
                        "answers": []
                    };

                    //Title
                    var title = document.querySelector("#ya-question-detail h1[itemprop=name]");
                    if (title)
                        result.title = title.innerText;

                    //Question
                    var question = document.querySelector("#ya-question-detail span[itemprop=text]");
                    if (question)
                        result.question = question.innerHTML;

                    //Best answer
                    var best_answer = document.querySelector("#ya-best-answer span[itemprop=text]");
                    if (best_answer)
                        result.answers.push({
                            "text": best_answer.innerHTML,
                            "best": true
                        });

                    //Other answers
                    var answers = document.querySelectorAll("#ya-qn-answers span[itemprop=text]");
                    Array.prototype.forEach.call(answers, (answer) => {
                        result.answers.push({"text": answer.innerHTML});
                    });

                    //Send result to server side
                    c.emit("result", result);
                }, qid);
        }
    });
});
