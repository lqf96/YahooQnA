# Yahoo Q&A Crawler
Yahoo Q&A crawler (Part of DSA-P2)  
This program crawls Yahoo Q&As related to given keyword.

## Usage
* Before running, ensure that `python2.7`, `python2-devel`, `pip` and `virtualenv` are installed.
* Connect to internet and run `bootstrap.sh`, which initializes the crawling environment.
* Run `crawler.py [Keyword] [Q&A Amount]` to crawl Q&A related to given keyword.
* The result is stored at `data/[Keyword]`.

## License
The project is licensed under GNU GPLv3.
